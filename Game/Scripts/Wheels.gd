extends Spatial

export var rotation_speed = 200; 

func _process(delta):
	var rot = rotation_speed*delta
	
	if self.rotation_degrees.z + rot < -180:
		self.rotation_degrees.z = 180 - (rot - (180 + self.rotation_degrees.z))
	else:
		self.rotation_degrees.z -= rot

extends RigidBody

export var min_size = 0.8;
export var max_size = 2;

export var planet_octaves = 3
export var planet_period = 28.6
export var planet_persistence = 0.551
export var planet_lacunarity = 2

export var atmosphere_octaves = 5
export var atmosphere_period = 52
export var atmosphere_persistence = 0.205
export var atmosphere_lacunarity = 1.87

const planet_material_path = preload("res://assets/planet.material")
const atmosphere_material_path = preload("res://assets/atmosphere.material")

var atmosphere
var planet


func _ready():
	randomize()
	
	atmosphere = self.get_node("atmosphere")
	planet = self.get_node("planet")
	
	var size = rand_range(min_size, max_size)
	var color = Color(rand_range(0, 1), rand_range(0, 1), rand_range(0, 1), 1.0)
	
	planet.scale = Vector3(size, size, size)
	atmosphere.scale = Vector3(size*1.1, size*1.1, size*1.1)
	
	planet.material_override = planet_material_path.duplicate()
	atmosphere.material_override = atmosphere_material_path.duplicate()
	
	planet.material_override.set_shader_param("inner_color", color)
	atmosphere.material_override.set_shader_param("color", Color((color.r-0.2), color.g-0.2, color.b-0.2))


func _on_planet_body_entered(body):
	print(body)
	if body.is_in_group("bullet"):
		body.queue_free()
		self.queue_free()

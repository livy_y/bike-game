extends Spatial

export var current_controller = 1; # controller 1 is left and controller 2 is right

var bullet = preload("res://Scene/bullet.scn")

func _ready():
	if current_controller == 1:
		global.left_gun = self
	else:
		global.right_gun = self

func _on_Right_Hand_button_pressed(button):
	if current_controller == 1 and button == 15:
		spawn_bullet()


func _on_Right_Hand_button_release(button):
	if current_controller == 1 and button == 15:
		spawn_bullet()


func _on_Left_Hand_button_pressed(button):
	if current_controller == 2 and button == 15:
		spawn_bullet()


func _on_Left_Hand_button_release(button):
	if current_controller == 2 and button == 15:
		spawn_bullet()

func spawn_bullet():
	var curr_bullet = bullet.instance()
	curr_bullet.global_transform = self.get_node("spawningPoint").global_transform

	curr_bullet.linear_velocity = (self.get_node("spawningPoint").global_transform.origin - self.get_node("rotationCalculator").global_transform.origin)*Vector3(20, 20, 20)

	
	get_tree().get_root().add_child(curr_bullet)

extends Spatial

export var spawn_time = 0.8 # amount that spawn per minute

var time = 0
var planet = preload("res://Scene/planet.scn")

func _process(delta):
	time += delta
	
	if time > spawn_time:
		spawn_planet()
		
		time = 0

func spawn_planet():
	var curr_planet = planet.instance()
	curr_planet.translation = Vector3(rand_range(25, -25), rand_range(-25, 25), -30)
	
	add_child(curr_planet)
	

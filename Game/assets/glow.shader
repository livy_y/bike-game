shader_type spatial;

uniform sampler2D emission_teture;
uniform vec4 glow_color : hint_color = vec4(1.0);
uniform vec4 color : hint_color;

void fragment() {
	vec4 emission_color = texture(emission_teture,UV);
	
	ALBEDO = (emission_color + glow_color).xyz;
	EMISSION = (emission_color + glow_color).xyz;
}
#include "arduino.h"
#include "MillisTimer.h"

int const SENSOR_PIN = 13;
int count = 0;

// Create a timer that fires every 1000 milliseconds.
MillisTimer timer1 = MillisTimer(1000);

// This is the function that is called when the timer expires.
void myTimerFunction(MillisTimer &mt)
{
    Serial.println(count);
    count = 0;
    timer1.start();
}

void setup() {
    Serial.begin(115200);

    pinMode(SENSOR_PIN, INPUT_PULLUP);

    timer1.setInterval(1000);
    timer1.expiredHandler(myTimerFunction);
    timer1.setRepeats(5);
    timer1.start();
}

void loop() {
    timer1.run();

    if (digitalRead(SENSOR_PIN) == 1) {
        count++;
        delay(50);
    }
}